---
layout: page
title: About
permalink: /about/
---

## About Sam Chancey
---
<br>

<!--Insert MY Picture here-->

Hey there, Sam Chancey here, and welcome! 
This is A Wolf Among Programmers, where I post about my game projects, software development, and video games in general.

During the day, I am an Associate Developer at MPW Industrial Services, 
a company in Hebron, OH that offers a variety of services from industrial cleaning to water treatment.
I work mainly for their industrial water treatment branch as a Web Developer, but sometimes I get to work for the other services and even on mobile application!

<!--Insert Terminal Havok Picture here-->
Outside of my actual job, I strive to be an Indie Game Developer. At the moment, I have been focusing on developing games in Unity 
with my current game project called Terminal Havok. It is still in the early stages, and I am not sure I will keep the name.

Aside from work and game developement, I play a lot of games with my wife and friends. We play a couple of PC MMOs like 
[Warframe][Wf] and [Guild Wars 2][GuildWars2]. 
If you are ever in the mood to play with some enthusiast gamers, looking for a squad or a clan, feel free to contact me on [Steam](http://steamcommunity.com/id/rorazoro)!

For discussions other than to play games, visit the [contact](http://sdchancey91.gitlab.io/contact/) page to get in touch.

[Wf]: https://www.warframe.com
[GuildWars2]: https://www.guildwars2.com